package de.matoatoa.zuul.webservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

/**
 * @author Jan Bormann
 */
@RestController
@SpringBootApplication
class WebServiceApplication{
    @GetMapping("/hallo")
    fun halloWelt () = "Hallo Welt!"

    @GetMapping("/hallo/{name}")
    fun hallo(@PathVariable name : String) = "Hallo $name!"
}

fun main(args: Array<String>) {
    SpringApplication.run(WebServiceApplication::class.java, *args)
}
