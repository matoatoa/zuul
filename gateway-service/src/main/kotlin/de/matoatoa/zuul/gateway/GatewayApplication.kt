package de.matoatoa.zuul.gateway

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * @author Jan Bormann
 */

@SpringBootApplication
@RestController
class GatewayApplication{
    @GetMapping("/", "")
    fun response () = "42"
}

fun main(args: Array<String>) {
    SpringApplication.run(GatewayApplication::class.java, *args)
}
