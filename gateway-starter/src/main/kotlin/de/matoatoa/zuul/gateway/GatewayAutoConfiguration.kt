package de.matoatoa.zuul.gateway

import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import de.matoatoa.fiftynine.zuul.gatewaystarter.GatwayProperties
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.web.ServerProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import org.springframework.cloud.netflix.zuul.filters.SimpleRouteLocator
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.util.Base64Utils.encodeToString

/**
 * @author Jan Bormann
 */
@Configuration
@EnableZuulProxy
@EnableConfigurationProperties(GatwayProperties::class)
class GatewayAutoConfiguration{

    @Bean
    @ConditionalOnProperty(prefix = "matoatoa.zuul", value = ["enabled"], matchIfMissing = true)
    fun test(serverProperties: ServerProperties, zuulProperties: ZuulProperties, gatwayProperties: GatwayProperties) =
            SimpleRouteLocator(serverProperties.servletPrefix, zuulProperties.apply {
                this.routes["proxy"] = ZuulProperties.ZuulRoute(gatwayProperties.path, gatwayProperties.target)
            })

    @Bean
    @ConditionalOnProperty(prefix = "matoatoa.zuul", value = ["enabled"], matchIfMissing = true)
    fun basicAuthFilter (properties: GatwayProperties) = object : ZuulFilter(){
        override fun run() = RequestContext.getCurrentContext()
                .addZuulRequestHeader("Authorization", "Basic " + "${properties.user}:${properties.password}".encodeToBase64())

        override fun shouldFilter() = true

        override fun filterType() = "pre"

        override fun filterOrder() = 10
    }

    private fun String.encodeToBase64() = encodeToString(this.toByteArray())
}