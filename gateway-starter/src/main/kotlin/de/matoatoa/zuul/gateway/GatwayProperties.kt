package de.matoatoa.fiftynine.zuul.gatewaystarter

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * @author Jan Bormann
 */
@ConfigurationProperties(prefix = "matoatoa.zuul")
data class GatwayProperties( var enabled: Boolean = true,
                             var path : String = "/proxy/**",
                             var target : String = "http://localhost:9001/hallo",
                             var user: String = "admin",
                             var password : String = "secret")